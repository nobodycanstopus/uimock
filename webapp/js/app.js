var app = angular.module('backloss',[
    'ngTagsInput',
    'ngRoute',
    'ngAnimate',
    'ui.bootstrap',
    'ngMap',
    'angularFileUpload',
    'duScroll',
    'xeditable',
    'pascalprecht.translate',
    '720kb.socialshare',
    'angular-clipboard',
    'ui.grid',
    'ui.grid.selection'
]);

app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('httpInterceptor');
}]);

app.run(function(editableOptions)   {
    editableOptions.theme = 'bs3';
});

app.factory('UrlLanguageStorage', ['$location', function($location) {
    return {
        set: function (name, value) {},
        put: function(name, value) {},
        get: function (name) {
            return $location.search()['lang']
        }
    };
}]);

app.config(function ($translateProvider) {
    $translateProvider
        .useStaticFilesLoader({
            prefix: 'locale-',
            suffix: '.json'
        });
    //$translateProvider.useUrlLoader('messageBundle');
    $translateProvider.useStorage('UrlLanguageStorage');
    var locale = getCookie('lang');
    locale = locale != undefined ? locale : 'en';
    $translateProvider.preferredLanguage(locale);
    $translateProvider.fallbackLanguage(locale);
});

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    setTimeout(function() {
        $('.dropdown_item').find('.dropdown-toggle').attr('data-toggle','dropdown');
    }, 1000);

    $(document).on('click', '.navbar-nav a', function() {
        $('#backloss-navbar').collapse('hide');
    });

    $('#backloss-navbar').find('.dropdown-menu').addClass('menu-responsive');
}

$(document).on('click', '.map-close', function() {
    $(this).closest('.fade').trigger('click');
});
