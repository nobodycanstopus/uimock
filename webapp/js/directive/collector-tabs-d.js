app.directive('collectorTabs', function(){
    return {
        restrict : 'E',
        templateUrl : './html/collector/tabs.html',
        controller : ['$scope', function($scope) {
            $scope.isPillActive = function(path) {
                return path == window.location.hash;
            }
        }]
    }
});