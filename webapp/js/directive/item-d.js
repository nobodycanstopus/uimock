app.directive('item', function ($filter) {
        return {
        restrict: 'E',
        templateUrl: './html/item-template.html',
        link: function ($scope, $el, $attr) {
            $scope.itemType = $attr['itemtype'];
            $scope.headerText = $filter('translate')('add.' + $scope.itemType + '.header');
            $scope.belowHeaderText = $filter('translate')('add.' + $scope.itemType + '.invite');
        },
        controller: ['$scope', '$http', 'itemService', '$timeout', '$rootScope', '$filter',
            function ($scope, $http, itemService, $timeout, $rootScope, $filter) {

                if ($scope.user.id == undefined) {
                    window.location.href = '#login';
                    return;
                }

                function parseTags(userInput) {
                    var re = /(?:^|\W)#([\wа-яA-Я]+)(?!\w)/g, match, matches = [];
                    while (match = re.exec(userInput)) {
                        var tag = match[1];
                        if (matches.indexOf(tag) == -1) {
                            matches.push(tag);
                        }
                    }
                    return matches;
                }

                function clearPhotoInput() {
                    var previewImages = document.getElementById('drag-area').getElementsByClassName('upload-preview-wrapper');
                    for (var i = previewImages.length - 1; i >= 0; i--)
                        previewImages[i].parentNode.removeChild(previewImages[i]);
                }

                function alert(msg) {
                    $scope.alertClass = 'alert-warning';
                    $scope.alertText = msg;
                    $scope.link = '';
                }

                function showProgress() {
                    $scope.$parent.shadow = "shadow";
                    $scope.showProgress = true;
                    console.log('Saving: ' + $scope.item.tags);
                }

                function alertSuccess() {
                    $scope.alertClass = 'alert-success';
                    $scope.alertText = $filter('translate')('add.success');
                    $scope.link = $filter('translate')('add.success.link');
                }

                function alertInviteToCheckEmail() {
                    $scope.alertClass = 'alert-success';
                    $scope.alertText = $filter('translate')('add.checkmail');
                }

                function alertPhotoIsTooBig() {
                    $scope.alertClass = 'alert-danger';
                    $scope.alertText = $filter('translate')('add.photo.too.big');
                }

                function clearInput() {
                    $scope.files = [];
                    $scope.images = [];
                    $scope.userText = undefined;
                    clearPhotoInput();
                }

                function hideProgress() {
                    $scope.$parent.shadow = "";
                    $scope.showProgress = false;
                }

                function alertError() {
                    $scope.alertClass = 'alert-danger';
                    $scope.alertText = $filter('translate')('error.general');
                    $scope.link = '';
                }

                function valid(email) {
                    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    return re.test(email);
                }

                $scope.$on("tooBigPhoto", function(){
                    alertPhotoIsTooBig();
                    $timeout(function () {
                        $scope.hideAlert()
                    }, 10000);
                });

                $scope.save = function () {
                    if (emailRequired()) {
                        return;
                    }
                    fetchUserInput();
                    if (!validLocation()) {
                        return;
                    }
                    if (!validTags()) {
                        return;
                    }
                    showProgress();
                    sendToServer();
                    switchOffAlerts();

                    function emailRequired() {
                        if (!$scope.user.valid && !valid($scope.user.email)) {
                            alert($filter('translate')('msg.requiredEmail'));
                            return true;
                        } else {
                            return false;
                        }
                    }

                    function fetchUserInput() {
                        var userInput = $scope.userText;
                        $scope.item.tags = parseTags(userInput);
                        $scope.item.description = userInput;
                    }

                    function validLocation() {
                        if (!$scope.item.location || !$scope.item.latitude || !$scope.item.longitude) {
                            alert($filter('translate')('msg.requiredLocation'));
                            return false;
                        } else {
                            return true;
                        }
                    }

                    function validTags() {
                        if ($scope.item.tags.length == 0) {
                            alert($filter('translate')('msg.requiredDescription'));
                            return false;
                        } else {
                            return true;
                        }
                    }

                    function sendToServer() {
                        $scope.item.type = $scope.itemType;
                        itemService.save($scope.item, $scope.images, $scope.user.email)
                            .then(function (id) {
                                $scope.savedId = id;
                                if ($scope.user.valid) {
                                    alertSuccess();
                                } else {
                                    alertInviteToCheckEmail();
                                }
                                clearInput();
                                return navigator.geolocation.getCurrentPosition(function (pos) {
                                    $scope.item = itemService.create(pos.coords.latitude, pos.coords.longitude);
                                    itemService.resolveLocation(pos.coords.latitude, pos.coords.longitude, $scope.userLang).then(function (location) {
                                        $scope.item.location = location;
                                        hideProgress()
                                    });
                                });
                            }).catch(function () {
                                alertError();
                                hideProgress();
                            });
                    }

                    function switchOffAlerts() {
                        $timeout(function () {
                            $scope.hideAlert()
                        }, 10000);
                    }


                };

                $scope.locationChanged = function () {
                    var coordinates = this.getPlace().geometry.location;
                    $scope.item.latitude = coordinates.lat();
                    $scope.item.longitude = coordinates.lng();
                };

                $scope.hideAlert = function () {
                    $scope.alertClass = 'invisible';
                };

                $scope.hideAlert();
                $scope.files = [];
                $scope.images = [];
                return navigator.geolocation.getCurrentPosition(function (pos) {
                    $scope.item = itemService.create(pos.coords.latitude, pos.coords.longitude);
                    itemService.resolveLocation(pos.coords.latitude, pos.coords.longitude, $scope.userLang).then(function (location) {
                        $scope.item.location = location;
                    });
                });
            }]
    }
});