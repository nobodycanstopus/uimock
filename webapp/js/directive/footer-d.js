app.directive('backlossFooter', function($uibModal){
   return {
       restrict : 'E',
       templateUrl : './html/footer.html',
       controller : ['$scope', function($scope) {


           $scope.openAddFeedbackPopup = function() {
               if ($scope.user.id == undefined) {
                   window.location.href = '#login';
                   return;
               }

               var feedbackModal = $uibModal.open({
                   animation: true,
                   templateUrl: 'html/add-feedback-popup.html',
                   controller: 'feedbackPopupController',
                   size: 'md',
                   scope: $scope
               });
               feedbackModal.result.then(function () {
               })
           };
       }]
   }
});