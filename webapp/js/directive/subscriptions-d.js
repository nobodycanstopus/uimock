app.directive('subscriptions', function($uibModal, subscriptionService){
   return {
       restrict : 'E',
       templateUrl : './html/subscriptions.html',
       controller : ['$scope', function($scope) {

           $scope.editSubscription = function (id) {
               var itemModal = $uibModal.open({
                   animation: true,
                   templateUrl: 'html/edit-subscription-popup.html',
                   controller: 'editSubscriptionPopupController',
                   size: 'md',
                   resolve: {
                       id: function () {
                           return id
                       }
                   }
               });
               itemModal.result.then(function () {
                   subscriptionService.userSubscriptions().then(function(subscriptionList) {
                       $scope.subscriptionList = subscriptionList;
                   });
               })
           };

           $scope.removeSubscription = function (id) {
               var itemModal = $uibModal.open({
                   animation: true,
                   templateUrl: 'html/delete-subscription-popup.html',
                   controller: 'deleteSubscriptionPopupController',
                   size: 'md',
                   resolve: {
                       id: function () {
                           return id
                       }
                   }
               });
               itemModal.result.then(function () {
                   subscriptionService.userSubscriptions().then(function(subscriptionList) {
                       $scope.subscriptionList = subscriptionList;
                   });
               })
           };

           subscriptionService.userSubscriptions().then(function(subscriptionList) {
               $scope.subscriptionList = subscriptionList;
           });
       }]
   }
});