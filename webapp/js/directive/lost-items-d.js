app.directive('lostItems', function ($uibModal, $http, itemService) {
    return {
        restrict: 'E',
        templateUrl: './html/lost-items.html',
        controller: ['$scope', '$http', 'itemService',
            function ($scope, $http, itemService) {

                function load() {
                    itemService.findForUser('lost').then(function (data) {
                        $scope.lostItems = data;
                        setTimeout(function () {
                            $('#fh5co-board-lost').masonry({
                                itemSelector: '.item',
                                columnWidth: 285,
                                gutterWidth: 20
                            });
                        }, 1000);
                    });
                }

                $scope.openItem = function (id) {
                    var itemModal = $uibModal.open({
                        animation: true,
                        templateUrl: 'html/lost-item-popup.html',
                        controller: 'lostItemPopupController',
                        size: 'md',
                        resolve: {
                            id: function () {
                                return id
                            }
                        }
                    });
                    itemModal.result.then(function () {
                        load();
                    })
                };

                load();
            }]
    }
});