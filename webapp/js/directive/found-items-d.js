app.directive('foundItems', function ($uibModal, $http, itemService) {
    return {
        restrict: 'E',
        templateUrl: './html/found-items.html',
        controller: ['$scope', '$http', 'itemService',
            function ($scope, $http, itemService) {

                function load() {
                    itemService.findForUser('found').then(function (data) {
                        $scope.foundItems = data;
                        setTimeout(function () {
                            $('#fh5co-board').masonry({
                                itemSelector: '.item',
                                columnWidth: 285,
                                gutterWidth: 20
                            });
                        }, 1000);
                    });
                }

                $scope.openItem = function (id) {
                    var itemModal = $uibModal.open({
                        animation: true,
                        templateUrl: 'html/item-popup.html',
                        controller: 'itemPopupController',
                        size: 'md',
                        resolve: {
                            id: function () {
                                return id
                            }
                        }
                    });
                    itemModal.result.then(function () {

                        load();
                    })
                };

                load();
            }]
    }
});