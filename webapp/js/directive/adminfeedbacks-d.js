app.directive('backlossAdminfeedbacks', function () {
    return {
        restrict: 'E',
        templateUrl: './html/adminfeedbacks.html',
        controller: ['$scope', '$http', '$uibModal', '$timeout', function ($scope, $http, $uibModal, $timeout) {

            $scope.adminFeedbacks = [];
            $scope.currentAdminFeedback;
            $scope.currentAFIndex = 0;
            $scope.editmode = false;

            $scope.getadminfeedbacks = function () {
                var workItems = $http.get('admin/feedbacks')
                    .then(function (response) {
                        return response.data;
                    });
                return workItems;
            };

            $scope.changeMode = function(){
                $scope.editmode = !$scope.editmode;
            }

            $scope.getadminfeedbacks().then(function (fbList) {
                $scope.adminFeedbacks = fbList;
                if ($scope.adminFeedbacks.length <= 0)
                    return;
                $scope.currentAFIndex = 0;
                $scope.currentAdminFeedback = $scope.adminFeedbacks[$scope.currentAFIndex];
            });

            $scope.getFINavClass = function (feedback) {
                if (feedback == $scope.currentAdminFeedback)
                    return "selected-fi-nav-item";
                return "";
            }

            $scope.previousFI = function () {
                if ($scope.currentAFIndex == 0) {
                    $scope.currentAFIndex = $scope.adminFeedbacks.length - 1;
                } else {
                    $scope.currentAFIndex = $scope.currentAFIndex - 1;
                }
                $scope.currentAdminFeedback = $scope.adminFeedbacks[$scope.currentAFIndex];
            };

            $scope.nextFI = function () {
                if ($scope.currentAFIndex >= $scope.adminFeedbacks.length - 1) {
                    $scope.currentAFIndex = 0;
                } else {
                    $scope.currentAFIndex = $scope.currentAFIndex + 1;
                }
                $scope.currentAdminFeedback = $scope.adminFeedbacks[$scope.currentAFIndex];
            };

            $scope.saveFI = function () {
                if (false/*$scope.editmode*/) { //TODO do work in controller
                    $http.post('admin/savefi',$scope.currentAdminFeedback).then(function (response) {
                        $scope.alertSuccess("Success: feedback saved");
                        $scope.adminFeedbacks.splice($scope.adminFeedbacks.indexOf($scope.currentAdminFeedback), 1);
                        $scope.updateCurrent();
                        $scope.needMore();
                        $timeout(function () {
                            $scope.hideAlert()
                        }, 1000);
                    });
                }
                else {
                    $http.post('admin/enablefeedback?id='+$scope.currentAdminFeedback.id)
                        .then(function (response) {
                            $scope.alertSuccess("Success: feedback enabled");
                            $scope.adminFeedbacks.splice($scope.adminFeedbacks.indexOf($scope.currentAdminFeedback), 1);
                            $scope.updateCurrent();
                            $scope.needMore();
                            $timeout(function () {
                                $scope.hideAlert()
                            }, 1000);
                        });
                }
            }

            $scope.updateCurrent = function(){
                if($scope.adminFeedbacks.length - 1 <  $scope.currentAFIndex){
                    $scope.currentAFIndex = $scope.currentAFIndex - 1;
                }
                $scope.currentAdminFeedback = $scope.adminFeedbacks[$scope.currentAFIndex];
            }

            $scope.deleteFI = function () {
                $http.post('admin/deletefeedback?id='+$scope.currentAdminFeedback.id)
                    .then(function (response) {
                        $scope.alertSuccess("Success: feedback enabled");
                        $scope.adminFeedbacks.splice($scope.adminFeedbacks.indexOf($scope.currentAdminFeedback), 1);
                        $scope.updateCurrent();
                        $scope.needMore();
                        $timeout(function () {
                            $scope.hideAlert()
                        }, 1000);
                    });
            }


        }]
    }
});