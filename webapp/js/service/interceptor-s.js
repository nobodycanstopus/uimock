app.factory('httpInterceptor', ['$rootScope','$q', function($rootScope,$q) {

    return {
        // optional method
        'request': function(config) {
            // do something on success
            console.log('request');
            return config;
        },

        // optional method
        'requestError': function(rejection) {
            // do something on error
            console.log('requestErrir');
            return $q.reject(rejection);
        },



        // optional method
        'response': function(response) {
            // do something on success
            console.log('response');
            return response;
        },

        // optional method
        'responseError': function(rejection) {
            // do something on error
            console.log('responseError');

            return $q.reject(rejection);
        }
    };
}]);
