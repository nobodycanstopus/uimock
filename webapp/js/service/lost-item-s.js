app.factory('lostItemService', ['$http', function ($http) {
    var lostItemService = {
        update: function (item, images) {
            var data = new FormData();
            if(images) {
                for(var i = 0; i < images.length; i++) {
                    data.append("images", images[i]);
                }
            }
            data.append('item', new Blob([JSON.stringify(item)], {type: 'application/json; charset=UTF-8'}));
            return $http({
                url: 'lost/update',
                method: 'POST',
                data: data,
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function (response) {
                return response.data;
            });
        },
        find: function (id) {
            return $http.get('lost/find/' + id).then(function (response) {
                return response.data;
            });
        }
    };
    return lostItemService;
}]);

