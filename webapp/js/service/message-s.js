app.factory('messageService', ['$http', function ($http) {
    return {
        sendToOwner: function (itemId, message) {
            var params = $.param({itemId: itemId, message: message});
            return $http({
                method: 'POST',
                url: 'message/send',
                data: params,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function (response) {
                return response.data;
            });
        },
        send: function (dialogId, message) {
            var params = $.param({message: message});
            return $http({
                method: 'POST',
                url: 'message/send/' + dialogId,
                data: params,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function (response) {
                    return response.data;
                },
                function (response) {
                    return response;
                });
        },
        getDialogs: function() {
            return $http.get('mocks/dialogs.json').then(function (response) {
                return response.data;
            });
        },
        getDialog: function(id) {
            return $http.get('mocks/messages.json').then(function (response) {
                return response.data;
            });
        },
        timeSince: function(timestmp) {
            if (timestmp == undefined) {
                return "Now";
            }

            var seconds = Math.floor((new Date() - new Date(timestmp)) / 1000);

            var interval = Math.floor(seconds / 31536000);

            if (interval > 1) {
                return interval + " years";
            }
            interval = Math.floor(seconds / 2592000);
            if (interval > 1) {
                return interval + " months";
            }
            interval = Math.floor(seconds / 86400);
            if (interval > 1) {
                return interval + " days";
            }
            interval = Math.floor(seconds / 3600);
            if (interval > 1) {
                return interval + " hours";
            }
            interval = Math.floor(seconds / 60);
            if (interval > 1) {
                return interval + " minutes";
            }
            return Math.floor(seconds) + " seconds";
        }
    };
}]);

