app.factory('feedbackService', ['$http', function ($http) {
    var feedbackService = {
        leaveFeedback: function (feedback) {
            var feedbackService = $http.post('feedback/add', feedback).then(function (response) {
                return response.data;
            });
            return feedbackService;
        },
        list: function (offset) {
            var feedbackService = $http.get('mocks/feedbacks.json').then(function (response) {
                return response.data;
            });
            return feedbackService;
        }
    };

    return feedbackService;
}]);

