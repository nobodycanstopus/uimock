app.factory('subscriptionService', ['$http', function ($http) {
    var subscriptionService = {
        subscribe: function(category) {
            var subscriptionPromise = $http.post('subscription/add', category).then(function (response) {
                return response.data;
            });
            return subscriptionPromise;
        },
        update: function(subscription) {
            var subscriptionPromise = $http.post('subscription/update', subscription).then(function (response) {
                return response.data;
            });
            return subscriptionPromise;
        },
        userSubscriptions: function() {
            var userSubscriptionPromise = $http.get('lost/current/user').then(function (response) {
                return response.data;
            });
            return userSubscriptionPromise;
        },
        find: function(id) {
            var userSubscriptionPromise = $http.get('subscription/find/' + id).then(function (response) {
                return response.data;
            });
            return userSubscriptionPromise;
        },
        remove: function(id) {
            var userSubscriptionPromise = $http.get('subscription/delete/' + id).then(function (response) {
                return response.data;
            });
            return userSubscriptionPromise;
        }
    };

    return subscriptionService;
}]);

