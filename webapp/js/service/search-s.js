app.factory('searchService', ['$http', function ($http) {
    var searchService = {
        search: function (tagArray, lat, lng) {
            var tags = [];

            tagArray.forEach(function (tag) {
                tags.push(tag.text.replace('#',''));
            });

            var searchPromise = $http.get('mocks/search.json')
            .then(function (response) {
                return response.data;
            });
            return searchPromise;
        },
        searchLatest: function (lat, lng, offset) {
            var latest = $http.get('mocks/search.json')
                .then(function (response) {
                    return response.data;
                });
            return latest;
        },
        searchMore: function (tagArray, lat, lng, offset) {
            var tags = [];

            tagArray.forEach(function (tag) {
                tags.push(tag.text)
            });

            var searchPromise = $http.get('found/search?tags='
            + tags.join(',') + '&lat=' + lat + '&lng=' + lng + '&offset=' + offset)
                .then(function (response) {
                    return response.data;
                });
            return searchPromise;
        },
        autoComplete: function (query) {
            var autoCompletePromise = $http.get('autocomplete/' + query).then(function (response) {
                return response.data;
            });
            return autoCompletePromise;
        }
    };
    return searchService;
}]);
