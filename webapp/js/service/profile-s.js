app.factory('profileService', ['$http', function ($http) {
    return {
        validate: function() {
            return $http.get('profile/validate').then(function (response) {
                return response.data;
            });
        },
        getProfile: function() {
            return $http.get('mocks/profile.json').then(function (response) {
                return response.data;
            });
        },
        deactivate: function() {
            return $http.get('user/deactivate').then(function (response) {
                return response.data;
            });
        }
    };
}]);
