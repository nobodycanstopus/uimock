app.factory('publicService', ['$http', function ($http) {
    return {
        getInfo: function(network, socialPublicId) {
            return $http.get('admin/public/info/' + network + '/' + socialPublicId).then(function (response) {
                return response;
            });
        },
        refresh: function(id) {
            return $http.get('admin/public/refresh/' + id).then(function (response) {
                return response;
            });
        },
        add: function(data) {
            return $http.post('admin/public/add', data).then(function (response) {
                return response;
            });
        },
        searchByTitle: function(title) {
            return $http.get('admin/public/search?title='+title).then(function (response) {
                return response;
            });
        },
        delete: function(id) {
            return $http.delete('admin/public/delete/' + id).then(function (response) {
                return response;
            });
        },
        disable: function(id) {
            return $http.put('admin/public/disable/' + id).then(function (response) {
                return response;
            });
        },
        enable: function(id) {
            return $http.put('admin/public/enable/' + id).then(function (response) {
                return response;
            });
        }
    };
}]);

