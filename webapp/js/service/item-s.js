app.factory('itemService', ['$http', function ($http) {
    var itemService = {
        create: function (lat, lng) {
            var item = {};
            item.tags = [];
            item.photoUrls = [];
            item.latitude = lat;
            item.longitude = lng;
            item.description = "";
            return item;
        },
        save: function (item, images, email) {
            var data = new FormData();
            if(images) {
                for(var i = 0; i < images.length; i++) {
                    data.append("images", images[i]);
                }
            }
            data.append('item', new Blob([JSON.stringify(item)], {type: 'application/json; charset=UTF-8'}));
            if (email != undefined) {
                data.append('email', new Blob([email], {type: 'application/text; charset=UTF-8'}));
            }
            return $http({
                url: item.type + '/save',
                method: 'POST',
                data: data,
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function (response) {
                return response.data;
            });
        },
        update: function (item, images) {
            var data = new FormData();
            if(images) {
                for(var i = 0; i < images.length; i++) {
                    data.append("images", images[i]);
                }
            }
            data.append('item', new Blob([JSON.stringify(item)], {type: 'application/json; charset=UTF-8'}));
            return $http({
                url: 'found/update',
                method: 'POST',
                data: data,
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function (response) {
                return response.data;
            });
        },
        resolveRegion: function (lat, lng, lang) {
            var regionPromise = $http.get('http://maps.googleapis.com/maps/api/geocode/json?latlng='
            + lat + ',' + lng + '&sensor=true&language=' +lang).then(function (response) {
                if (response.data.results.length != 0) {
                    return response.data.results[0].address_components[2].short_name;
                }
            });
            return regionPromise;
        },
        resolveLocation: function (lat, lng, lang) {
            var locationPromise = $http.get('http://maps.googleapis.com/maps/api/geocode/json?latlng='
            + lat + ',' + lng + '&sensor=true&language='+lang).then(function (response) {
                if (response.data.results.length != 0) {
                    return response.data.results[0].formatted_address;
                }
            });
            return locationPromise;
        },
        find: function (id) {
            return $http.get('found/find/' + id).then(function (response) {
                return response.data;
            });
        },
        findForUser: function (itemType) {
            return $http.get(itemType + '/current/user').then(function (response) {
                return response.data;
            });
        }
    };
    return itemService;
}]);

