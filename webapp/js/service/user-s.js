app.factory('userService', ['$http', function ($http) {
    var user = {
        id: null,
        authorized: false,
        name: undefined,
        providerId: undefined,
        avatarUrl: undefined,
        connections: undefined,
        valid: undefined,
        email: ''
    };

    return {
        create: function (data) {
            user.id = data.id;
            user.authorized = true;
            user.name = data.displayName;
            user.avatarUrl = data.avatarUrl;
            user.valid = data.valid;
            user.email = (!data.valid && data.email != undefined) ? data.email : '';
            user.lang = data.lang;
            return user;
        }
    }
}]);