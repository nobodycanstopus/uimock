app.config(['$routeProvider',function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'html/intro.html'
        })
        .when('/feedback', {
            templateUrl: 'html/feedbacks.html'
        })
        .when('/error', {
            templateUrl: 'html/error.html'
        })
        .when('/login', {
            templateUrl: 'html/login.html'
        })
        .when('/messages', {
            templateUrl: 'html/messages.html'
        })
        .when('/notifications', {
            templateUrl: 'html/notify.html'
        })
        .when('/search', {
            templateUrl: 'html/search.html'
        })
        .when('/add', {
            templateUrl: 'html/add.html'
        })
        .when('/lost', {
            templateUrl: 'html/add-lost.html'
        })
        .when('/about', {
            templateUrl: 'html/about.html'
        })
        .when('/admin', {
            templateUrl: 'html/admin.html'
        })
        .when('/collector', {
            templateUrl: 'html/collector/collector.html'
        })
        .when('/collector/public', {
            templateUrl: 'html/collector/public.html'
        })
        .when('/profile', {
            templateUrl: 'html/profile.html'
        })
        .when('/recover/error', {
            templateUrl: 'html/recover-error.html'
        })
        .when('/recover/:token', {
            templateUrl: 'html/recover.html'
        })
        .when('/activate/:status', {
            templateUrl: 'html/activation.html'
        })
        .when('/activation/send', {
            templateUrl: 'html/send-activation.html'
        })
        .when('/activation/send/error/:error', {
            templateUrl: 'html/send-activation.html'
        })
        .when('/validate/:status', {
            templateUrl: 'html/validation.html'
        })
        .when('/recover/error/:error', {
            templateUrl: 'html/recover.html'
        })
        .when('/forgot', {
            templateUrl: 'html/forgot.html'
        })
        .when('/changeEmail', {
            templateUrl: 'html/change-email.html'
        })
        .when('/forgot/:error', {
            templateUrl: 'html/forgot.html'
        })
        .when('/changeEmail/error/:error', {
            templateUrl: 'html/change-email.html'
        })
        .when('/login/:error', {
            templateUrl: 'html/login.html'
        })
        .when('/register/:status', {
            templateUrl: 'html/register.html'
        })
        .when('/register', {
            templateUrl: 'html/register.html'
        })
        .when('/item/:id', {
            templateUrl: 'html/item.html'
        }).otherwise({
            redirectTo: '/'
        });
}]);