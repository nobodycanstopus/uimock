app.controller('feedbackController', ['$scope', 'feedbackService', '$uibModal', function ($scope, feedbackService, $uibModal) {
    $scope.listFeedbacks = function() {
        feedbackService.list($scope.offset).then(function(data){
            $scope.hasMore = data.length > 10;
            if($scope.hasMore) {
                $scope.offset = data.length - 1;
                data.pop()
            } else {
                $scope.offset = 0;
            }
            $scope.feedbacks = data;
        });
    };

    $scope.moreFeedbacks = function () {
        feedbackService.list($scope.offset)
            .then(function (data) {
                $scope.hasMore = data.length > 10;
                if($scope.hasMore) {
                    $scope.offset += data.length - 1;
                    data.pop();
                }
                $scope.feedbacks = $scope.feedbacks.concat(data);
            });
    };

    $scope.openAddFeedbackPopup = function() {
        if ($scope.user.id == undefined) {
            window.location.href = '#login';
            return;
        }

        var feedbackModal = $uibModal.open({
            animation: true,
            templateUrl: 'html/add-feedback-popup.html',
            controller: 'feedbackPopupController',
            size: 'md',
            scope: $scope
        });
        feedbackModal.result.then(function () {
        })
    };

    $scope.hasMore = false;
    $scope.offset = 0;
    $scope.listFeedbacks();
}]);