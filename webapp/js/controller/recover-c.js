app.controller('recoverController', ['$scope', '$routeParams', function ($scope, $routeParams) {
    $scope.showError = false;
    $scope.token = $routeParams.token;

    if ($routeParams.error != undefined) {
        $scope.showError = true;
        $scope.errorCode = $routeParams.error;
    }

}]);