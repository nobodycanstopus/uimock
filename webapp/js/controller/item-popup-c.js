app.controller('itemPopupController', function ($scope, $uibModalInstance, $http, $timeout, id, itemService) {

    $scope.saved = false;
    function parseTags(userInput) {
        var re = /(?:^|\W)#(\w+)(?!\w)/g, match, matches = [];
        while (match = re.exec(userInput)) {
            matches.push(match[1]);
        }
        return matches;
    }

    function render(files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                (function (curr) {
                    var reader = new FileReader();
                    reader.onload = function () {
                        var dataURL = reader.result;
                        var area = document.getElementById('drag-area');
                        var div = document.createElement('div');
                        div.className = "upload-preview-wrapper";
                        var cross = document.createElement('span');
                        cross.className = "removeCross";
                        cross.onclick = function (e) {
                            e.stopPropagation();
                            $scope.item.photoUrls.splice(curr, 1);
                            for (var n = 0; n < $scope.files.length; n++)
                                if ($scope.files[n].name === files[curr].name) {
                                    $scope.files.splice(n, 1);
                                    break;
                                }
                            div.remove();
                        };
                        var img = document.createElement('img');
                        img.className = 'upload-preview';
                        img.src = dataURL;
                        div.appendChild(img);
                        div.appendChild(cross);
                        area.appendChild(div);
                    };
                    reader.readAsDataURL(files[curr]);
                })(i);
            }
        }
    }

    $scope.files = [];

    itemService.find(id).then(function (item) {
        $scope.item = item;
        $scope.initPhotoInput($scope.item.photoUrls);
    });

    $scope.no = function () {
        $scope.yes = false;
        $uibModalInstance.dismiss('cancel');
    };

    $scope.yes = function () {
        $scope.yes = true;
        $uibModalInstance.close();
    };

    $scope.locationChanged = function () {
        var coordinates = this.getPlace().geometry.location;
        $scope.item.latitude = coordinates.lat();
        $scope.item.longitude = coordinates.lng();
    };

    $scope.save = function () {
        $scope.item.tags = parseTags($scope.item.description);
        // TODO: implement update method in service using save as an example
        itemService.update($scope.item, $scope.images).then(function () {
            alertSuccess("Saved");
            $scope.saved = true;
            angular.element( document.querySelector( '#saveButton' ) );
            $timeout(function () {
                $scope.hideAlert()
            }, 2000);
        });
    };

    $scope.initPhotoInput = function (urls) {
        var files = [];
        for (var i = 0; i < urls.length; i++) {
            (function (i) {
                var req = new XMLHttpRequest();
                req.open("GET", urls[i], true);
                req.responseType = "arraybuffer";
                req.onload = function (e) {
                    var arrayBufferView = new Uint8Array(this.response);
                    var file = new Blob([arrayBufferView], {type: 'image/jpeg'});
                    files.push(file);
                    if (i === (urls.length - 1)) {
                        render(files);
                    }
                };
                req.send();
            })(i);
        }
    };

    function alertSuccess(message) {
        $scope.alertClass = 'alert-success';
        $scope.alertText = message;
    }

    function alertError(message) {
        $scope.alertClass = 'alert-danger';
        $scope.alertText = message;
        $scope.link = '';
    }

    $scope.hideAlert = function () {
        $scope.alertClass = 'invisible';
    };


});