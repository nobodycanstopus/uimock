app.controller('feedbackPopupController', ['$scope', '$uibModalInstance', 'feedbackService', '$timeout', '$filter',
    function ($scope, $uibModalInstance, feedbackService, $timeout, $filter) {

        function alert(msg) {
            $scope.alertClass = 'alert-warning';
            $scope.alertText = msg;
        }

        function alertSuccess() {
            $scope.alertClass = 'alert-success';
            $scope.alertText = $filter('translate')('feedback.add.success');
        }

        function alertError() {
            $scope.alertClass = 'alert-danger';
            $scope.alertText = $filter('translate')('error.general');
        }

        function hideAlert() {
            $scope.alertClass = 'invisible';
            $scope.alertText = '';
        }

        $scope.yes = function () {
            $uibModalInstance.close();
        };

        $scope.no = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.leaveFeedback = function () {
            if (!$scope.feedback.summary) {
                alert($filter('translate')('feedback.summary.required'));
                return;
            }
            if (!$scope.feedback.content) {
                alert($filter('translate')('feedback.content.required'));
                return;
            }
            feedbackService.leaveFeedback($scope.feedback)
                .then(function () {
                    alertSuccess();
                    $scope.feedbackSaved = true;
                    $timeout(function () {
                        hideAlert();
                        $scope.yes();
                    }, 5000)
                }).catch(function () {
                    alertError();
                    $timeout(function () {
                        hideAlert();
                    }, 5000);
                });
        };

        if ($scope.user.id == undefined) {
            window.location.href = '#login';
            return;
        }
        $scope.feedbackSaved = false;
        $scope.feedback = {};

    }
]);