app.controller('authController', ['$scope', '$http', 'userService','$rootScope',
    function ($scope, $http, userService, $rootScope) {
        $scope.user = {};
        $http.get('mocks/user.json').success(
            function (data) {
                $scope.user = userService.create(data);
                if ($scope.user.lang != $scope.userLang) {
                    $scope.user.lang = $scope.userLang;
                    $http.get('user/lang/change/' + $scope.userLang);
                }
            }
        );

        $scope.$watch('user', function(newUser, oldUser){
            if (newUser.authorized) {
                //connectMessageSocket();
                console.log('connect to web-socket');
            }
        }, true);

        $rootScope.stompClient = null;

        function connectMessageSocket() {
            var socket = new SockJS('/backloss/send');
            $rootScope.stompClient = Stomp.over(socket);
            $rootScope.stompClient.connect({}, function(frame) {
                console.log('Connected: ' + frame);
                $rootScope.stompClient.subscribe('/topic/messages/' + $scope.user.id, function(message){
                    console.log("something received : " + message);
                });
            });
        }

    }]);