app.controller('subscriptionController', ['$scope', 'subscriptionService', 'searchService', '$timeout', '$filter',
    function ($scope, subscriptionService, searchService, $timeout, $filter) {

        function fetchTags(tags) {
            var tagsStrArr = [];
            for (var i = 0; i < tags.length; i++) {
                (function (i) {
                    tagsStrArr.push(tags[i].text)
                })(i);
            }
            return tagsStrArr;
        }

        function alert(msg) {
            $scope.alertClass = 'alert-warning';
            $scope.alertText = msg;
        }

        function alertSuccess() {
            $scope.alertClass = 'alert-success';
            $scope.alertText = $filter('translate')('subscriptions.add.success');
        }

        function alertError() {
            $scope.alertClass = 'alert-danger';
            $scope.alertText = $filter('translate')('error.general')
        }

        function hideAlert() {
            $scope.alertClass = 'invisible';
            $scope.alertText = '';
        }

        $scope.yes = function () {
            $uibModalInstance.close();
        };

        $scope.no = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.subscribe = function () {
            if ($scope.tagsToSubscribe.length == 0) {
                alert($filter('translate')('subscriptions.tag.required'));
                return;
            }
            if (!$scope.subscription.location) {
                alert($filter('translate')('subscriptions.location.required'));
                return;
            }
            $scope.subscription.tags = fetchTags($scope.tagsToSubscribe);
            $scope.subscription.email=$scope.user.email;
            subscriptionService.subscribe($scope.subscription)
                .then(function () {
                    alertSuccess();
                    $scope.subscribed = true;
                    $timeout(function () {
                        hideAlert();
                        $scope.yes();
                    }, 5000)
                }).catch(function () {
                    alertError();
                    $timeout(function () {
                        hideAlert();
                    }, 5000);
                });
        };

        if ($scope.user.id == undefined) {
            window.location.href = '#login';
            return;
        }

        $scope.locationChanged = function () {
            var coordinates = this.getPlace().geometry.location;
            $scope.subscription.latitude = coordinates.lat();
            $scope.subscription.longitude = coordinates.lng();
        };

        $scope.complete = function ($query) {
            return searchService.autoComplete($query).then(function (data) {
                return data;
            });
        };

        $scope.subscribed = false;
        $scope.subscription = {};
        $scope.tagsToSubscribe = angular.copy($scope.tags);
        $scope.subscription.location = angular.copy($scope.location);
        $scope.subscription.latitude = angular.copy($scope.latitude);
        $scope.subscription.longitude = angular.copy($scope.longitude);

    }
]);