app.controller('uploadController', ['$scope', function ($scope) {
    $scope.$watch('files', function () {
        if ($scope.files && $scope.files.length) {
            $scope.upload($scope.files);
        }
    });

    $scope.upload = function (files) {
        for (var i = 0; i < files.length; i++) {
            if(files[i].size > 7000000) {
                $scope.$emit("tooBigPhoto");
                files.splice(i,1);
            }
        }
        for (var i = 0; i < files.length; i++) {
            if ($scope.images.length < 4) {
                (function (curr) {
                var reader = new FileReader();
                reader.onload = function () {
                    var dataURL = reader.result;
                    var area = document.getElementById('drag-area');
                    var div = document.createElement('div');
                    div.className = "upload-preview-wrapper";
                    var cross = document.createElement('span');
                    cross.className = "removeCross";
                    cross.onclick = function (e) {
                        e.stopPropagation();
                        for (var n = 0; n < $scope.images.length; n++)
                            if ($scope.images[n].name === files[curr].name) {
                                $scope.images.splice(n, 1);
                                div.remove();
                                break;
                            }
                    };
                    var img = document.createElement('img');
                    img.className = 'upload-preview';
                    img.src = dataURL;
                    div.appendChild(img);
                    div.appendChild(cross);
                    area.appendChild(div);
                };
                reader.readAsDataURL(files[curr]);
            })(i);
                $scope.images.push(files[i]);
            }
        }
    }
}]);
