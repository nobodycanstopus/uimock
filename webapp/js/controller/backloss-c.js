app.controller('backlossController', function($scope, $http, $filter, $uibModal) {

    $scope.errors = {
        1: $filter('translate')('errors.1'),
        2: $filter('translate')('errors.2'),
        3: $filter('translate')('errors.3'),
        4: $filter('translate')('errors.4'),
        5: $filter('translate')('errors.5'),
        6: $filter('translate')('errors.6'),
        7: $filter('translate')('errors.7'),
        8: $filter('translate')('errors.8'),
        9: $filter('translate')('errors.9'),
        invalidToken: $filter('translate')('errors.invalidToken')
    };

    $scope.showProgress = false;
    $scope.userLang = getCookie('lang') || window.navigator.language || window.navigator.userLanguage || 'en';

    $scope.$on('languageChanged', function(event, newLang) {
        $scope.userLang = newLang;
    });

    $scope.infoPopup = function (obj) {
        var infoModal = $uibModal.open({
            animation: true,
            templateUrl: 'html/common-popup.html',
            controller: 'commonPopupController',
            size: 'md',
            resolve: {
                obj: function () {
                    return obj
                }
            }
        });
        infoModal.result.then(function () {
            alert(1);
        })
    };

    console.log("user lang - " + $scope.userLang);
});