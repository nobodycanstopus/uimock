    app.controller('loginController', ['$scope', '$routeParams', function ($scope, $routeParams) {
    $scope.showError = false;

    if ($routeParams.error != undefined) {
        $scope.showError = true;
        $scope.errorCode = $routeParams.error;
    }

}]);