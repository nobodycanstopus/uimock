app.controller('adminController', ['$scope', '$http', '$uibModal', '$timeout', function ($scope, $http, $uibModal, $timeout) {
    $scope.workItems = [];
    $scope.currentItemIndx;
    $scope.currentItem;
    $scope.adminpanels = ["disabledItems","feedbacks"];
    $scope.admintool =  $scope.adminpanels[0];



    $scope.getToolClass = function(toolName){
        if(toolName ==  $scope.admintool){
            return "selected-admin-tool"
        }
        return ""
    }

    $scope.changeTool = function (admintool) {
        $scope.admintool = admintool;
    };

    $scope.openPhoto = function (photoUrl) {
        var photoModal = $uibModal.open({
            animation: true,
            templateUrl: 'html/photo-popup.html',
            controller: 'photoPopupController',
            size: 'md',
            resolve: {
                photoUrl: function () {
                    return photoUrl
                }
            }
        });
        photoModal.result.then(function () {

        })
    };

    $scope.deletePhoto = function (photoUrl) {
        for (var i = 0; i < $scope.currentItem.item.photoUrls.length; i++) {
            if ($scope.currentItem.item.photoUrls[i] == photoUrl) {
                $scope.currentItem.item.photoUrls.splice(i, 1);
                break;
            }
        }
    };

    $scope.deleteTag = function (tag) {
        if($scope.currentItem.item.tags.length<2){
            $scope.alertError("Delete tag ERROR : Item has only 1 tag");
            $timeout(function () {
                $scope.hideAlert()
            }, 3000);
            return;
        }
        for (var i = 0; i < $scope.currentItem.item.tags.length; i++) {
            if ($scope.currentItem.item.tags[i] == tag) {
                $scope.currentItem.item.tags.splice(i, 1);
                break;
            }
        }
    };

    $scope.alertSuccess = function (message) {
        $scope.alertClass = 'alert-success';
        $scope.alertText = message;
    }

    $scope.alertError = function(message) {
        $scope.alertClass = 'alert-danger';
        $scope.alertText = message;
        $scope.link = '';
    }

    $scope.hideAlert = function () {
        $scope.alertClass = 'invisible';
    };

    $scope.addTag = function (tag) {
        $scope.currentItem.item.tags.push(tag);
    };

    $scope.needMore = function  (){
        if($scope.workItems.length == 0){
            $scope.getworkitems().then(function (workItems) {
                $scope.workItems = workItems;
                $scope.currentItemIndx = 0;
                $scope.currentItem = workItems[$scope.currentItemIndx];
            });
        }
    }

    $scope.getworkitems = function () {
        var workItems = $http.get('admin/workitems')
            .then(function (response) {
                return response.data;
            });
        return workItems;
    };

    /*use to get next  work items*/
    $scope.getworkitems().then(function (workItems) {
        $scope.workItems = workItems;
        $scope.currentItemIndx = 0;
        $scope.currentItem = workItems[$scope.currentItemIndx];
    });

    $scope.previousWI = function () {
        if ($scope.currentItemIndx == 0) {
            $scope.currentItemIndx = $scope.workItems.length - 1;
        } else {
            $scope.currentItemIndx = $scope.currentItemIndx - 1;
        }
        $scope.currentItem = $scope.workItems[$scope.currentItemIndx];
    };

    $scope.nextWI = function () {
        if ($scope.currentItemIndx >= $scope.workItems.length - 1) {
            $scope.currentItemIndx = 0;
        } else {
            $scope.currentItemIndx = $scope.currentItemIndx + 1;
        }
        $scope.currentItem = $scope.workItems[$scope.currentItemIndx];
    };

    $scope.updateCurrent = function(){
        if($scope.workItems.length - 1 <  $scope.currentItemIndx){
            $scope.currentItemIndx = $scope.currentItemIndx - 1;
        }
        $scope.currentItem = $scope.workItems[$scope.currentItemIndx];
    }


    $scope.saveWI = function () {
        var data = new FormData();
        data.append('adminItem', new Blob([JSON.stringify($scope.currentItem)], {type: 'application/json; charset=UTF-8'}));
        $http({
            url: 'admin/savewi',
            method: 'POST',
            data: data,
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function (response) {
            $scope.alertSuccess("Success: Item saved");
            $scope.workItems.splice($scope.workItems.indexOf($scope.currentItem), 1);
            $scope.updateCurrent();
            $scope.needMore();
            $timeout(function () {
                $scope.hideAlert()
            }, 1000);
        });
    };

    $scope.removeWI = function () {
        var data = new FormData();
        data.append('adminItem', new Blob([JSON.stringify($scope.currentItem)], {type: 'application/json; charset=UTF-8'}));
        $http({
            url: 'admin/removewi',
            method: 'POST',
            data: data,
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function (response) {
            $scope.alertSuccess("Success: Item removed");
            $scope.workItems.splice($scope.workItems.indexOf($scope.currentItem), 1);
            $scope.updateCurrent();
            $timeout(function () {
                $scope.hideAlert()
            }, 1000);
            $scope.needMore();
        });
    };

    $scope.getNavItemClass = function(navItem){
        if ($scope.workItems.indexOf(navItem) == $scope.currentItemIndx){
            return "nav-item-selected";
        }
        return "";
    };

    $scope.selectItem = function(navItem){
        $scope.currentItemIndx = $scope.workItems.indexOf(navItem);
        $scope.currentItem = navItem;
    };

}]);