app.controller('languageController', ['$scope','$translate','$location', '$http', function($scope, $translate, $location, $http) {

    $scope.changeLanguage = function (locale) {
        if($scope.userLang === locale) {
            return;
        }
        $scope.userLang = locale;
        $scope.$emit('languageChanged', locale);
        $translate.use(locale);
        $location.search('lang', locale);
        window.location.reload();
    };
}]);