app.controller('registerController', ['$scope', '$routeParams', function ($scope, $routeParams) {
    $scope.showError = false;

    if ($routeParams.status != undefined) {
        if ($routeParams.status == 'success') {
            $scope.success = true;
        } else {
            $scope.showError = true;
            $scope.errorCode = $routeParams.status;
        }
    }
}]);