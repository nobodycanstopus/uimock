app.controller('messageController', function ($scope, $http, $routeParams, $rootScope, messageService) {
    $scope.conversation = null;
    $scope.dialog = null;
    $scope.message = "";
    $scope.oppositeUserId=null;

    $scope.getDialogs = function() {
        messageService.getDialogs().then(function (dialogs) {
            $scope.dialogs = dialogs;
        });
    };

    $scope.getDialog = function(id) {
        messageService.getDialog(id).then(function (dialog) {
            $scope.conversation = dialog;
            $scope.oppositeUserId=dialog.userId;
            $scope.conversation.with = ($scope.user.id == dialog.userId) ? $scope.user.name : dialog.usersInfo[dialog.userId].displayName;
        });
    };

    $scope.writeMessage = function () {
        messageService.send($scope.conversation.dialogId, $scope.message).then(
            function (response) {
                $rootScope.stompClient.send("/app/send/" + $scope.oppositeUserId, {}, JSON.stringify({message: $scope.message, sender: $scope.user.id}));
                $scope.conversation.messages.push({message: $scope.message, sender: $scope.user.id});
                $scope.message = "";
            },
            function (error) {
                console.log(error[data].error);
            }
        );
    };

    $scope.timeSince = function (timestmp) {
        return messageService.timeSince(timestmp);
    }

    $scope.getDialogs();

});