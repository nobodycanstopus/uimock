app.controller('editSubscriptionPopupController',
    function ($scope, $http, $routeParams, $uibModalInstance, id, subscriptionService, searchService, $timeout) {

        function alert(msg) {
            $scope.alertClass = 'alert-warning';
            $scope.alertText = msg;
        }

        function alertSuccess() {
            $scope.alertClass = 'alert-success';
            $scope.alertText = $filter('translate')('subscriptions.edit.success');
        }

        function alertError() {
            $scope.alertClass = 'alert-danger';
            $scope.alertText = $filter('translate')('errors.general');
        }

        function hideAlert() {
            $scope.alertClass = 'invisible';
            $scope.alertText = '';
        }

        $scope.yes = function () {
            $uibModalInstance.close();
        };

        $scope.no = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.update = function () {
            if ($scope.subscription.tags == 0) {
                alert($filter('translate')('subscriptions.tag.required'));
                return;
            }
            if (!$scope.subscription.location) {
                alert($filter('translate')('subscriptions.location.required'));
                return;
            }
            subscriptionService.update($scope.subscription).then(function () {
                alertSuccess();
                $scope.subscriptionUpdated = true;
                $timeout(function () {
                    hideAlert();
                    $scope.yes();
                }, 5000)
            }).catch(function () {
                alertError();
                $timeout(function () {
                    hideAlert();
                }, 5000);
            })
        };

        $scope.complete = function ($query) {
            return searchService.autoComplete($query).then(function (data) {
                return data;
            });
        };

        $scope.locationChanged = function () {
            var coordinates = this.getPlace().geometry.location;
            $scope.subscription.latitude = coordinates.lat();
            $scope.subscription.longitude = coordinates.lng();
        };

        subscriptionService.find(id).then(function (subscription) {
            $scope.subscription = subscription;
        });
    });

