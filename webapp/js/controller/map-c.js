app.controller('mapController', function($scope, NgMap, itemService) {

    $scope.marker = {};
    $scope.marker.coordinates = [];
    $scope.marker.coordinates[0] = $scope.latitude;
    $scope.marker.coordinates[1] = $scope.longitude;

    $scope.$on('mapInitialized', function (event, map) {
        window.dlgMap = map;
        window.setTimeout(function(){
            google.maps.event.trigger(map, 'resize');
            map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
        },150);

    });

    $scope.placeChanged = function() {
        $scope.marker = {};
        $scope.marker.coordinates = [];
        $scope.marker.coordinates[0] = this.getPlace().geometry.location.lat();
        $scope.marker.coordinates[1] = this.getPlace().geometry.location.lng();
        NgMap.getMap().then(function(map) {
            map.setCenter(this.getPlace().geometry.location);
        });
        itemService.resolveRegion($scope.marker.coordinates[0], $scope.marker.coordinates[1], $scope.userLang).then(function(newLocation){
            var position = {};
            position.latitude = $scope.marker.coordinates[0];
            position.longitude = $scope.marker.coordinates[1];
            position.newLocation = newLocation;
            $scope.$emit("locationChanged", position);
        });
    };



});