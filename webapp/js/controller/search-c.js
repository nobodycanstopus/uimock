app.controller('searchController', ['$scope', '$http', '$uibModal', 'searchService', 'itemService', '$window',
    function ($scope, $http, $uibModal, searchService, itemService, $window) {

        $scope.showDetails = function(id){
            $window.open('#/item/'+id);
        };

        $scope.searchMore = function () {
            searchService.searchMore($scope.tags, $scope.latitude, $scope.longitude, $scope.offset)
                .then(function (items) {
                    $scope.hasMore = items.length > 20;
                    if($scope.hasMore) {
                        $scope.offset += items.length - 1;
                        items.pop();
                    }
                    $scope.items = $scope.items.concat(items);
                });
        };

        $scope.search = function () {
            if ($scope.tags.length == 0) {
                return;
            }

            if (!$scope.location) {
                $scope.openMap();
            }

            searchService.search($scope.tags, $scope.latitude, $scope.longitude)
                .then(function (items) {
                    $scope.hasMore = items.length > 10;
                    if($scope.hasMore) {
                        $scope.offset = items.length - 1;
                        items.pop();
                    } else {
                        $scope.offset = 0;
                    }
                    $scope.items = items;
        /*            angular.element('#fh5co-board').masonry({
                        itemSelector: '.item',
                        gutter: 20
                    });*/
                });
        };

        $scope.foundSomething = function() {
            return $scope.items.length === 0;
        };

        $scope.openMap = function () {
            var itemModal = $uibModal.open({
                animation: true,
                templateUrl: 'html/map.html',
                scope: $scope,
                size: 'lg'
            });
            itemModal.result.then(function (location) {
                $scope.location = location;
            });
            setTimeout(function() {
                var modalDialog = $('.modal-dialog'),
                    modalBody = $('.modal-body'),
                    dialogTopMargin = parseInt(modalDialog.css('margin-top')),
                    dialogBottomMargin = parseInt(modalDialog.css('margin-bottom')),
                    bodyTopPadding = parseInt(modalBody.css('padding-top')),
                    bodyBottomPadding = parseInt(modalBody.css('padding-bottom')),
                    searchFormHeight = 43;
                    $('.s-map').css('height',($(window).height()-dialogTopMargin-dialogBottomMargin-bodyTopPadding-bodyBottomPadding-searchFormHeight+"px"));
            }, 1000);
        };

        $scope.complete = function ($query) {
            return searchService.autoComplete($query).then(function (data) {
                return data;
            });
        };

        $scope.$on('locationChanged', function (event, newPosition) {
            $scope.location = newPosition.newLocation;
            $scope.latitude = newPosition.latitude;
            $scope.longitude = newPosition.longitude;
        });

        $scope.tags = [];
        $scope.offset = 0;
        $scope.hasMore = false;

        navigator.geolocation.getCurrentPosition(function (pos) {
            $scope.latitude = pos.coords.latitude;
            $scope.longitude = pos.coords.longitude;
            if($scope.latitude && $scope.longitude) {
                searchService.searchLatest($scope.latitude, $scope.longitude, $scope.offset)
                    .then(function (items) {
                        $scope.hasMore = items.length > 20;
                        if($scope.hasMore) {
                            $scope.offset = items.length - 1;
                            items.pop();
                        } else {
                            $scope.offset = 0;
                        }
                        $scope.items = items;
                        /*            angular.element('#fh5co-board').masonry({
                         itemSelector: '.item',
                         gutter: 20
                         });*/
                    });
            }
            itemService.resolveRegion(pos.coords.latitude, pos.coords.longitude, $scope.userLang).then(function (location) {
                $scope.location = location ? location : 'What is your location?';
            });
        });
    }]);
