app.controller('profileController', ['$scope', '$http', 'profileService', '$filter', '$uibModal',
    function ($scope, $http, profileService, $filter, $uibModal) {

        $scope.getProfile = function () {
            profileService.getProfile().then(function (profile) {
                $scope.profile = profile;
            });
        };

        $scope.validateMail = function () {
            profileService.validate().then(function () {
                $scope.infoPopup({
                    data: $filter('translate')('profile.mail.sent'),
                    title: $filter('translate')('profile.mail.sent.title')
                });
            })
        };

        $scope.openDeactivatePopup = function() {
            var deactivateModal = $uibModal.open({
                animation: true,
                templateUrl: 'html/deactivate-profile-popup.html',
                controller: 'deactivateProfilePopupController',
                size: 'md',
                scope: $scope
            });
            deactivateModal.result.then(function () {

            })
        };

        $scope.getProfile();

    }]);