app.controller('validateController', ['$scope', '$routeParams', function ($scope, $routeParams) {

    $scope.showError = false;

    if ($routeParams.status != undefined) {
        if($routeParams.status === 'success') {
            $scope.user.valid = true;
        }
        $scope.showError = $routeParams.status == 'error'
    }

}]);