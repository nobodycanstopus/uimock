app.controller('collectorController', ['$scope', '$http', '$uibModal', '$timeout','collectorService', function ($scope, $http, $uibModal, $timeout, collectorService) {

    $scope.collect = function() {
        collectorService.collect().then(
            function (response) {
                console.log(response);
            },
            function (error) {
                console.log(error);
            }
        );
    }

}]);