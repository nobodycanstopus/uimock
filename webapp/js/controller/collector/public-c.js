app.controller('publicController',
    ['$scope', '$http', '$uibModal', '$timeout','publicService',
        function ($scope, $http, $uibModal, $timeout, publicService) {
            $scope.socialPublicId = "";
            $scope.network = "facebook";
            $scope.current = null;
            $scope.searchPublic = "";

            $scope.refreshEnabled = false;
            $scope.deleteEnabled = false;
            $scope.editEnabled = false;
            $scope.disableEnabled = false;
            $scope.enableEnabled= false;

            $scope.gridOptions = {
                enableRowSelection: true,
                enableSorting: true,
                columnDefs: [
                    { name: 'id', displayName: 'Id'  },
                    { name: 'providerId', displayName: 'Network'  },
                    { name: 'socialPublicId', displayName: 'Network Id' },
                    { name: 'title', displayName: 'Title', enableSorting: false },
                    { name: 'followers', displayName: 'Followers', enableSorting: false},
                    { name: 'enabled', displayName: 'Enabled'}
                ],
                onRegisterApi: function( gridApi ) {
                    $scope.gridApi = gridApi;

                    gridApi.selection.on.rowSelectionChanged($scope, function(row){
                        $scope.refreshEnabled =
                            $scope.deleteEnabled =
                                $scope.editEnabled = gridApi.selection.getSelectedGridRows().length > 0;

                        $scope.enableEnabled = gridApi.selection.getSelectedGridRows().length == 1 && !gridApi.selection.getSelectedGridRows()[0].entity.enabled;
                        $scope.disableEnabled = gridApi.selection.getSelectedGridRows().length == 1 && gridApi.selection.getSelectedGridRows()[0].entity.enabled;
                    });
                }
            };

            $scope.delete = function() {
                var id = $scope.gridApi.selection.getSelectedGridRows()[0].entity.id;
                publicService.delete(id).then(
                    function (response) {
                        $scope.searchByTitle();
                    },
                    function (error) {
                        console.log(error);
                    }
                );
            };

            $scope.enable = function() {
                var entity = $scope.gridApi.selection.getSelectedGridRows()[0].entity;
                if (entity.enabled) {
                    publicService.enable(entity.id).then(
                        function (response) {
                            $scope.searchByTitle();
                        },
                        function (error) {
                            console.log(error);
                        }
                    );
                }
            };

            $scope.disable = function() {
                var entity = $scope.gridApi.selection.getSelectedGridRows()[0].entity;
                if (entity.enabled) {
                    publicService.disable(entity.id).then(
                        function (response) {
                            $scope.searchByTitle();
                        },
                        function (error) {
                            console.log(error);
                        }
                    );
                }
            };

            $scope.edit = function() {

            };


            $scope.refreshSocialData = function() {
                var id = $scope.gridApi.selection.getSelectedGridRows()[0].entity.id;
                publicService.refresh(id).then(
                    function (response) {
                        $scope.current = response.data;
                    },
                    function (error) {
                        console.log(error);
                    }
                );
            };

            $scope.getInfo = function() {
                publicService.getInfo($scope.network, $scope.socialPublicId).then(
                    function (response) {
                        $scope.current = response.data;
                    },
                    function (error) {
                        console.log(error);
                    }
                );
            };

            $scope.searchByTitle = function() {
                publicService.searchByTitle($scope.searchPublic).then(
                    function (response) {
                        $scope.gridOptions.data = response.data;
                    },
                    function (error) {
                        console.log(error);
                    }
                );
            };

            $scope.add = function() {
                publicService.add($scope.current).then(
                    function (response) {
                        $scope.infoPopup({title: "Success", data: "Social public " + $scope.current.id + " saved to database"});
                        $scope.current = null;
                        $scope.socialPublicId = "";
                    },
                    function (error) {
                        console.log(error);
                    }
                );
            };

            $scope.searchByTitle();
        }
    ]
);