app.controller('sendActivationController', ['$scope', '$routeParams', function ($scope, $routeParams) {
    $scope.showError = false;
    $scope.showSuccess = false;

    if ($routeParams.error != undefined) {
        if (isError($routeParams.error)) {
            $scope.showError = true;
            $scope.errorCode = $routeParams.error;
        } else {
            $scope.showSuccess = true;
        }
    }

    function isError(error) {
        return error != 0;
    }
}]);