app.controller('commonPopupController', function ($scope, $uibModalInstance, $http, $timeout, obj) {
    $scope.message = obj.data;
    $scope.title = obj.title;

    $scope.no = function () {
        $scope.yes = false;
        $uibModalInstance.dismiss('cancel');
    };

    $scope.yes = function () {
        $scope.yes = true;
        $uibModalInstance.close();
    };
});