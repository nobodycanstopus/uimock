app.controller('deleteSubscriptionPopupController',
    function ($scope, $http, $routeParams, $uibModalInstance, id, subscriptionService, $timeout, $filter) {

        function alert(msg) {
            $scope.alertClass = 'alert-warning';
            $scope.alertText = msg;
        }

        function alertSuccess() {
            $scope.alertClass = 'alert-success';
            $scope.alertText = $filter('translate')('subscriptions.delete.success');
        }

        function alertError() {
            $scope.alertClass = 'alert-danger';
            $scope.alertText = $filter('translate')('errors.general');
        }

        function hideAlert() {
            $scope.alertClass = 'invisible';
            $scope.alertText = '';
        }

        $scope.yes = function () {
            $uibModalInstance.close();
        };

        $scope.no = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.remove = function () {
            subscriptionService.remove(id).then(function () {
                alertSuccess();
                $scope.subscriptionUpdated = true;
                $timeout(function () {
                    hideAlert();
                    $scope.yes();
                }, 5000)
            }).catch(function () {
                alertError();
                $timeout(function () {
                    hideAlert();
                }, 5000);
            })
        };

        subscriptionService.find(id).then(function (subscription) {
            $scope.subscription = subscription;
        });
    });

