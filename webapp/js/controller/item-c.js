app.controller('itemController', function ($scope, $http, $routeParams, itemService, messageService, clipboard) {


    $scope.message = "";

    $scope.writeMessage = function () {
        messageService.sendToOwner($scope.item.id, $scope.message).then(
            function (response) {
                $scope.message = '';
                $scope.showSuccess = true;
                $("#sendMessageSuccessAlert").fadeTo(2000, 500).slideUp(500, function(){
                    $("#sendMessageSuccessAlert").alert('close');
                    $scope.showSuccess = false;
                });
            },
            function (error) {
                $scope.infoPopup({title : error.data.title, data: error.data.error});
                console.log(error);
            }
        );

    };

    $scope.copyLink = function() {
        clipboard.copyText($scope.item.link);
    };

    itemService.find($routeParams.id).then(function (item) {
        $scope.item = item;

        $scope.marker = {};
        $scope.marker.coordinates = [];
        $scope.marker.coordinates[0] = $scope.item.latitude;
        $scope.marker.coordinates[1] = $scope.item.longitude;
    });
});