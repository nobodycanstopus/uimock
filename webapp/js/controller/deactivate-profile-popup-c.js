app.controller('deactivateProfilePopupController',
    function ($scope, $uibModalInstance, profileService, $timeout, $filter) {

        function alert(msg) {
            $scope.alertClass = 'alert-warning';
            $scope.alertText = msg;
        }

        function alertSuccess() {
            $scope.alertClass = 'alert-success';
            $scope.alertText = $filter('translate')('profile.delete.success');
        }

        function alertError() {
            $scope.alertClass = 'alert-danger';
            $scope.alertText = $filter('translate')('errors.general');
        }

        function hideAlert() {
            $scope.alertClass = 'invisible';
            $scope.alertText = '';
        }

        $scope.yes = function () {
            if($scope.profileDeactivated) {
                window.location.href = 'signout';
                return;
            }
            $uibModalInstance.close();
        };

        $scope.deactivate = function () {
            profileService.deactivate().then(function () {
                alertSuccess();
                $scope.profileDeactivated = true;
                $timeout(function () {
                    hideAlert();
                    window.location.href = 'signout';
                    return;
                }, 5000)
            }).catch(function () {
                alertError();
                $timeout(function () {
                    hideAlert();
                }, 5000);
            });
        };


        $scope.no = function () {
            $uibModalInstance.dismiss('cancel');
        };
    })
;

