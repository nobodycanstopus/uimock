<%@ page import="java.util.Arrays" %>
<%@ page import="com.backloss.core.bean.og.OpenGraphItem" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en" ng-app="backloss" ng-controller="backlossController">
<head>
    <meta property="og:type" content="${item.type}"/>
    <meta property="og:title" content="${item.title}"/>
    <meta property="og:description" content="${item.description}"/>
    <meta property="og:url" content="${item.url}"/>
    <c:if test="${item.image != null}">
        <meta property="og:image" content="${item.image}"/>
    </c:if>

    <meta property="article:published_time" content="${item.articlePublishedTime}" />
    <meta property="article:tag" content="<%=Arrays.asList(((OpenGraphItem)request.getAttribute("item")).getArticleTag())%>" />
</head>
<body>
</body>
</html>
